# Filename: <Makefile>
# Copyright (C) <2022> Authors: <Antoine Leroux>
# 
# This program is free software: you can redistribute it and / or 
# modify it under the terms of the GNU General Public License as published 
# by the Free Software Foundation, either version 2 of the License. 
# 
# This program is distributed in the hope that it will be useful, 
# but WITHOUT ANY WARRANTY; without even the implied warranty of  
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the  
# GNU General Public License for more details.  
# 
# You should have received a copy of the GNU General Public License  
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

path = docker
SHELL := /bin/bash
ID_CONTAINER:= $(shell docker ps --format "{{.ID}} {{.Image}}" | grep $$USER |  cut -f1 -d " ")
IMAGE_NAME:= $(shell docker ps --format "{{.ID}} {{.Image}}" | grep $$USER |  cut -f2 -d " ")

EXITED_CONTAINERS:= $(shell docker ps --quiet --filter=status=exited | xargs)

clean:
	docker system prune -a -f

clean-soft:
	@docker images -q |xargs docker rmi > /dev/null 2>&1 ; \
	@docker rmi $(docker images --filter "dangling=true" -q --no-trunc) > /dev/null 2>&1 ; \
	@docker rm $(EXITED_CONTAINERS) > /dev/null 2>&1 ; \
	echo "Cleaned your docker system !"

build:
	./build.sh $(CPATH)

run:
	./run.sh $(CPATH)

stop:
	@docker stop ${USER}-nvidia-triton-ai-srv-x86_64-container ; \
	echo "Container $(IMAGE_NAME) stopped !"

save:
	@docker commit $(ID_CONTAINER) $(IMAGE_NAME):latest ; \
	echo "Container $(IMAGE_NAME) saved !"
rm:
	docker rm ${USER}-nvidia-triton-ai-srv-x86_64-container

exec:
	docker exec -it ${USER}-nvidia-triton-ai-srv-x86_64-container bash

attach:
	docker attach ${USER}-nvidia-triton-ai-srv-x86_64-container