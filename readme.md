# Introduction

Repository made for running NVIDIA Triton Server with OpenCV CUDA, Aravis, RAPIDS, Tensorflow and all basic support on x86_64 architecture in a docker container.

Native support for SVS-VISTEK USB Cameras. See Setup for instruction for other cameras...

- **x86_64 architecture : Tested & Working container with X11Forwarding**

# Setup

## **Prerequisites**

- **You need to have a GPU compatible with CUDA >= 11.5 !**

- You need to have a correctly installed docker system with nvidia-docker2 package installed and configured. Please see [NVIDIA-DOCKER2 Setup](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#getting-started) for more details...

## Udev rules for Aravis Cameras

Create `70-aravis.rules` in `/etc/udev/rules.d` folder and insert the following.

```bash
SUBSYSTEM=="usb", ATTRS{idVendor}=="2c17", KERNEL=="ttyUSB[0-9]*", MODE:="0666", TAG+="uaccess", TAG+="udev-acl", SYMLINK+="svs_vistek"
```

Enter the correspond idVendor according to your `lsusb` output.

By default the SYMLINK argument is set to `svs_vistek`. If you want to change this, you need to edit `run.sh` on `-v "/dev/svs_vistek:/dev/svs_vistek"`.

Update then your udev rules : `sudo udevadm control --reload-rules && sudo udevadm trigger`

## X11 Forward for GL Apps

If using for a **remoteSSH server**, please make sure that in `/etc/ssh/sshd_config` you have the following :

```bash
X11Forwarding yes
X11DisplayOffset 10
X11UseLocalhost no
AllowAgentForwarding yes
AllowTcpForwarding yes
```

If it's not working, please consider running `xhost +local:docker` in a shell before launching the container.

# Launch

## **WARNING**

- You can run multiple commands at once.

- `make clean` command is dangerous in a way that it's cleaning your entire docker system by removing unused compiled images and containers.

- For instance, please consider that running `make build` will regen your container to its default state, thus wiping your modifications made with `make save`. A warning message is asking you confirmation either way.

## Available Commands

Please report to the `Makefile` for further documentation.

The available commands are :

- `make clean` : Please report to Warning section for use
- `make clean-soft` : Clean any exited container and remove unused images
- `make build CPATH=~/GIT` : Please report to Warning section for use. You can pass a path location to be bind to the container. <u>**DO NOT BIND YOUR HOME !!**</u>
- `make run CPATH=~/GIT` : Run the container in detached mode. You can pass a path location to be bind to the container. <u>**DO NOT BIND YOUR HOME !!**</u>
- `make save` : Save modifications made to the image and replace the current one
- `make rm` : Delete your container. **You need to rebuild it !**
- `make exec` : Open an bash instance in your already running container
- `make attach` : Attach your current terminal to the container. Meaning that when you're exiting it's stopping the container.

Press `CTRL+P & CTRL+Q` to detach container in CLI without stopping it.

It was choosen to use the same username inside the container instead of by default root user for monitoring purposes on server.

By proceding this way, files created or edited are labeled to your username which make things easier.

## Multiple commands

Basic usage could be :

- `make build run CPATH=~/GIT` : Build container and open a bash inside.
- `make save stop` : Save container and stop it.
